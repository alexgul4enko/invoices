import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import Root from './app/Root';
import configureStore from "./app/store/store";

const store = configureStore();
render(
	  <Root store={store} history={browserHistory} />,
	  document.getElementById('app'),
	);