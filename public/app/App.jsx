import React, { PropTypes } from 'react';
import Container from './container/Container';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './app.actions';

const  App = props=> {
  return (
    	<Container 
    		location = {props.location.pathname}
    		err = {props.err}
    		closeError = {props.dismissError}
            loading = {props.loading}
    	>
    		{props.children}
    	</Container> 
  )
}

App.propTypes= {
	children: PropTypes.element
}

const  mapStateToProps = (state= {}) =>({
    err: state.err,
    loading: state.loading,
})


const mapDispatchToProps = dispatch=> (bindActionCreators(actions,dispatch));


export default connect(mapStateToProps, mapDispatchToProps)(App);



