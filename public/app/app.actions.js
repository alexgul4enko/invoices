import Constances from './app.Constances';



const appActions = {
	dismissError:()=>(setError(''))
};


const setError = rehidrate=>({
	type:Constances.SHOW_ERR,
	rehidrate,
});

const showLoading = rehidrate=>({
	type:Constances.SHOW_LOADING,
});

const dismissLoading = rehidrate=>({
	type:Constances.DISMISS_LOADING,
});


export default appActions;
export  {setError,showLoading,dismissLoading}