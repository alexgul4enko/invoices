import React , {Component, PropTypes} from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';


export default class CustomersDialog extends Component{
	constructor(props){
		super(props);
		this.state = {
			name:'',
			address: '',
			phone:'',
			err:false,
		}
		this.tongleDialog = this.tongleDialog.bind(this);
	}

	render (){
		return (
			<Dialog
					active={this.props.active}
          			onOverlayClick={this.tongleDialog}
				>
					<Input 
         				type='text' 
         				label='Name' 
         				name='name'
         				icon='perm_identity'
         				value = {this.state.name}
         				onChange={this.handleCustChange.bind(this, 'name')} 
         				required
         				maxLength={50}
         				onKeyPress = {this.handleKey.bind(this, 'name')}
         				onBlur = {this.saveCustChange.bind(this,'name')} 
         				autoFocus
         				error = {this.state.err ? "field is required":null}
         				onFocus = {this.clearError.bind(this)}
         			/>
         			<Input 
         				type='text' 
         				label='Address' 
         				name='address'
         				icon='email'
         				value = {this.state.address}
         				onChange={this.handleCustChange.bind(this, 'address')} 
         				onKeyPress = {this.handleKey.bind(this, 'address')} 
         				onBlur = {this.saveCustChange.bind(this,'address')}
         				maxLength={150}
         				ref = "address"
         			/>
         			<Input 
         				type='tel' 
         				label='Phone' 
         				name='phone'
         				icon='phone'
         				value = {this.state.phone}
         				onChange={this.handleCustChange.bind(this, 'phone')} 
         				onKeyPress = {this.handleKey.bind(this, 'phone')} 
         				onBlur = {this.saveCustChange.bind(this,'phone')}
         				ref = 'phone'
         			/>

				</Dialog>
		)
	}

	
	tongleDialog(){
		this.setState({name:'',address: '',phone:''});
		this.props.tongleDialog();
	}
	clearError(){
		this.setState({err:false});
	}
	handleKey(type, e){
		const {which} = e;
		if(which ==13){
			const {name} = this.state;
			switch(type){
				case 'name':
					this.refs.address.getWrappedInstance().focus();
					return;
				case 'address':
					this.refs.phone.getWrappedInstance().focus();
					return ;
				case 'phone':
					if(name){
						this.saveCustChange('phone')
						this.tongleDialog();
					}
					return;
					
			}
		}
	}
	saveCustChange(type){
		const {inv} = this.props;
		const {name,address,phone} = this.state;
		if(name){
			this.props.editCustomer({name,address,phone,inv});
		}
		else{
			this.setState({err:true});
		}
		
	}
	handleCustChange(type,value){
		this.setState({[type]:value});
	}
}


CustomersDialog.deafultProps = {
	inv:{}
}

CustomersDialog.propTypes = {
	editCustomer: PropTypes.func.isRequired,
	inv: PropTypes.object,
	active:PropTypes.bool.isRequired,
	tongleDialog:PropTypes.func.isRequired,

}





