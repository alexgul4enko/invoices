import React , {Component, PropTypes} from 'react'

export default class EditText extends Component{
	constructor(props){
		super(props);
		this.state = {
			edit : false,
		}
		this.tongleEdit =this.tongleEdit.bind(this);
		this.renderContent = this.renderContent.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);

	}
	render (){
		const styles = {...componentStyles , ...this.props.style};
		return (
			<div className = {`EditDiv ${this.props.class}`} onClick = {this.tongleEdit} style = {styles} >
				{this.renderContent()}
			</div>
		)
		
	}
	tongleEdit() {
		if(!this.state.edit) this.setState({edit: !this.state.edit});
	}
	renderContent(){
		switch(this.state.edit){
			case true:
				return (
						<input 
							type={this.props.type} 
							defaultValue = {this.props.text}
							onBlur = {this.onChange}
							autoFocus
							style ={inputStyle}
							onKeyDown= {this.onKeyPress}
						/>
						)
			default :
						return `${this.props.before||''} ${this.props.text} ${this.props.after||''}`

		}
	}
	onKeyPress(e){
		switch(e.which){
			case 13:
				this.setState({edit:false});
				this.props.onChange(e.target.value);
				break;
			case 27:
				this.setState({edit:false});
				break;
		}
	}
	onChange(e){
		this.setState({edit:false});
		this.props.onChange(e.target.value);
	}

}


const componentStyles = {
	width : 150,
	height: 25,
	lineHeight: '25px',
	textAlign: 'center',
	// border: '1px solid white',
	cursor:'pointer',
	overflow:'hidden'
}

const inputStyle = {
	width : '100%',
	height : 'calc(100% - 2px)',
	background: 'transparent',
	border:0,
	padding: 0,
	borderBottom : '2px solid white',
	textAlign: 'center',
	color:'white',
}

EditText.defaultProps = {
	text: '',
	class: '',
	type:'email',
	style: {},
	after:'',
	before:''
}

EditText.propTypes = {
	onChange: PropTypes.func.isRequired,
	text: PropTypes.string,
	class: PropTypes.string,
	type: PropTypes.string,
	style : PropTypes.object,
	after: PropTypes.string,
	before: PropTypes.string,
}

