import React , {Component,PropTypes} from 'react';
import List from './List';
import ListHeader from './ListHeader';
import './list.css';

export default class UsersList extends Component {
	render (){
		return (
			<div className = {`FiltListContainer ${this.props.class}`}>
				{this.props.children}
			</div>
		)
	}
}
UsersList.defaultProps = {
	class:''
}
UsersList.propTypes = {
	class: PropTypes.string,
}

export {List, ListHeader};

