import React  from 'react';
import './loading.css';

const Loading = ()=>(
	<div className="loading">
		<div>
			<div className="c1"></div>
			<div className="c2"></div>
			<div className="c3"></div>
			<div className="c4"></div>
		</div>
		<span className='l'>loading...</span>
	</div>
)

export default  Loading ;