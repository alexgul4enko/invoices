import React, {Component, PropTypes} from 'react';
import EditText from '../EditText';
import './prod.css'

export default class Product extends Component{
	render(){
		return (
			<div className = "product_component">
				<EditText 
					text ={this.props.name}
					onChange= {this.changeName.bind(this)}
					style  = {{width:"calc(100% - 130px)", maxWidth:200, minWidth:150, color:'#394246', fontSize:"1.2em"}}
				/>
				<EditText 
					text ={this.props.price+''}
					onChange= {this.changePrice.bind(this)}
					type ="number"
					style  = {{width:80,fontSize:"1.2em",color:'#394246'}}
				/>
			</div>
		)
	}
	changeName(name){
		this.props.changeProd({...this.props.prod, name})
	}
	changePrice(price){
		this.props.changeProd({...this.props.prod, price:parseFloat(price)})
	}
	
}
Product.defaultProps ={
	name: '',
	price: 0,
}

Product.propTypes = {
	name: PropTypes.string,
	price: PropTypes.number,
	changeProd: PropTypes.func.isRequired,
}