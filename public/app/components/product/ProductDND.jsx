import React, {Component, PropTypes} from 'react';
import EditText from '../EditText';
import { DragSource, DropTarget  } from 'react-dnd';
import {compose} from 'redux';
import { findDOMNode } from 'react-dom';
import './prod.css'

const dndSource = {
  canDrag(props){
    if(props.hasOwnProperty("canDrug")){
      return props.canDrug;
    }
    return true;
  },
  beginDrag(props) {
    return {
      prod: props.prod,
    };
  },
  endDrag (props, monitor, component){
    
    if(props.onDrop){
      const source = monitor.getItem().prod;
      const target = monitor.getDropResult();
      props.onDrop( target,source)
    }
  }
};


@DragSource('product', dndSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  connectDragPreview: connect.dragPreview(),
}))
export default class ProductDND extends Component{
	render(){
		const {   connectDragSource ,connectDragPreview} = this.props;
		return connectDragPreview(
			<div className = "product_component">
				{
					connectDragSource(<span className = "material-icons">drag_handle</span>)
				}
				<EditText 
					text ={this.props.name}
					onChange= {this.changeName.bind(this)}
					style  = {{width:"calc(100% - 130px)", maxWidth:200, minWidth:150}}
				/>
				<EditText 
					text ={this.props.price+''}
					onChange= {this.changePrice.bind(this)}
					type ="number"
					style  = {{width:80}}
				/>
			</div>
		)
	}
	changeName(name){
		this.props.changeProd({...this.props.prod, name})
	}
	changePrice(price){
		this.props.changeProd({...this.props.prod, price:parseFloat(price)})
	}
}
ProductDND.defaultProps ={
	name: '',
	price: 0,
}

ProductDND.propTypes = {
	name: PropTypes.string,
	price: PropTypes.number,
	changeProd: PropTypes.func.isRequired,
	canDrug : PropTypes.bool.isRequired,
	connectDragSource: PropTypes.func,
}