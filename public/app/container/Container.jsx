import React, {Component,PropTypes} from 'react';
import Header from './components/header/Header'
import Menu from './components/menu/Menu';
import './container.css';
import WorkSpace from './components/workSpace/WorkSpace';
import {  Snackbar } from 'react-toolbox';

import Loading from '../components/loading/loading'


export default class Container extends Component{
	constructor(props){
		super(props);
		this.state = {
			show_menu:true,
		}
		this.tongleMenu = this.tongleMenu.bind(this);
		this.renderLoading = this.renderLoading.bind(this);
	}

	tongleMenu(){
		this.setState({show_menu : !this.state.show_menu});
	}
	renderLoading(){
		return this.props.loading ? (<div className = "app_loading">
								    	<Loading/>
								    </div>) : null;
	}

	render(){
		return (
			<div className = "appContainer">
				<Header 
					tongleMenu = {this.tongleMenu}
					tongle = {this.state.show_menu}
				/>
				<div className = "ContentWrapper">
					<Menu 
						tongle = {this.state.show_menu}
						items = {menuItems}
						location = {this.props.location }
					/>
					<WorkSpace 
						location = {this.props.location}
						tongle = {this.state.show_menu}
					>
						{this.props.children}
					</WorkSpace>
					
				</div>

				<Snackbar
					active={this.props.err!=''}
					label={this.props.err}
					timeout={1000}
					onTimeout={this.props.closeError}
					type='warning'
			      />
				{this.renderLoading()}
			</div>
		)
	}
}

Container.defaultProps = {
	err:'',
};
Container.propTypes ={
	err:PropTypes.string,
	closeError:PropTypes.func.isRequired,
	loading:PropTypes.bool.isRequired
};



const menuItems = [
	{icon:'local_grocery_store',title:'Invoices',name:'Invoices'},
	{icon:'people',title:'Customers',name:'Customers'},
	{icon:'shopping_basket',title:'Products',name:'Products'}
]
