import React, {Component, PropTypes} from 'react';
import './header.css';


const Header = props => (
	<div className = "header">
		<button 
			className = {`material-icons ${props.tongle ? 'hide': 'show'}`}
			onClick = {props.tongleMenu}
		>
			menu
		</button>
		<span>Invoices system</span>
	</div>
)
	

export default Header;











