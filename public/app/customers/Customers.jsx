import React , {PropTypes, Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './actions';
import './customers.css';
import {Button} from 'react-toolbox/lib/button';
import ListContainer, {List} from '../components/filtList/ListContainer';
import Customer from './components/Customer';
import NewCustomerDialog from './components/NewCustomerDialog';

class Customers extends Component {
	constructor(props){
		super(props);
		this.openCustomerDialod = this.openCustomerDialod.bind(this);
	}

	render(){
		return (
			<div className = "Content_divchik" id = "customers">
				<Button 
					className = "add_customer"
					icon='add' 
					label='New customer' 
					flat 
					onMouseUp = {this.openCustomerDialod}
					primary 
				/>
				<ListContainer class = "customers-list">
					<List
						filterPlaceHolder = "search"
						renderItem = {this.renderItem.bind(this)}
						items = {this.props.customers}
						filter = {this.filterContent}

					/>
				</ListContainer>

				<NewCustomerDialog 
					ref = "customer_dialog" 
					saveCustomer = {this.props.addCustomer}
				/>
			</div>
		)
	}

	openCustomerDialod(){
		this.refs.customer_dialog.openDialog();
	}
	renderItem(cust){
		return (
			<Customer
				key = {cust.id}
				{...cust}
				cust = {cust}
				editCustomer = {this.props.editCustomer}
			/>
		)
	}

	filterContent(item,filt){
		const regEx = new RegExp(filt, 'i');
		return regEx.test(JSON.stringify(item));
	}
	componentWillMount() {
		if(isArrEmpty(this.props.customers)) this.props.initData();
	}
}

const isArrEmpty = (arr=[])=>{
	return arr.length ==0;
}

const  mapStateToProps = (state= {}) =>({
    customers: state.customers,
})


const mapDispatchToProps = dispatch=> (bindActionCreators(actions,dispatch));


export default connect(mapStateToProps, mapDispatchToProps)(Customers);