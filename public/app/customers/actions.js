import Constances from './Constances'
import HttpPromice from '../HttpPromice';
import {showLoading,dismissLoading,setError} from '../app.actions';

const customersActions = {
	initData: ()=>{
		return (dispatch, getState)=>{
			dispatch(showLoading());
			HttpPromice('/api/customers',"GET")
				.then(products=>{
					dispatch(initCustomers(products));
					dispatch(dismissLoading());
				})
				.catch(err=>{
					dispatch(dismissLoading());
					dispatch(setError(err && err.message));
				})
		}

	},
	addCustomer: (cust={})=>{
		return (dispatch, getState)=>{
			const {address,name,phone} = cust;
			HttpPromice('/api/customers',"POST",{address,name,phone})
				.then(newCustomer=>{
					dispatch(addCustomer(newCustomer));
				})
				.catch(err=>{
						dispatch(setError(err && err.message));
				})
		}
	},
	editCustomer: (cust={})=>{
		return (dispatch, getState)=>{
			const {id, address,name,phone} = cust;
			HttpPromice(`/api/customers/${id}`,"PUT",{address,name,phone})
				.then(newCustomer=>{
					dispatch(updateCustomerData(newCustomer));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	}
}

const addCustomer = (rehidrate=[])=>({
	type:Constances.INSERT_CUSTOMER,
	rehidrate
});
const updateCustomerData = (rehidrate=[])=>({
	type:Constances.UPDATE_CUSTOMERDATA,
	rehidrate
});
const initCustomers = (rehidrate=[])=>({
	type:Constances.INIT_CUSTOMERS,
	rehidrate
});


export default customersActions;