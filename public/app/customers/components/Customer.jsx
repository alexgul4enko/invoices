import React , {PropTypes, Component} from 'react';
import EditText from '../../components/EditText';

export default class Customer extends Component {

	render (){
		const {cust} = this.props;
		return (<div className = "Customer-item">
					<div className = 'cust-row'>
						<span className = 'material-icons'>perm_identity</span>
						<EditText 
							text ={this.props.name}
							onChange= {this.handleChange.bind(this,'name',cust)}
							style  = {style}
						/>
					</div>
					<div className = 'cust-row'>
						<span className = 'material-icons'>mail</span>
						<EditText 
							text ={this.props.address}
							onChange= {this.handleChange.bind(this,'address',cust)}
							style  = {style}
						/>
					</div>
					<div className = 'cust-row'>
						<span className = 'material-icons'>phone</span>
						<EditText 
							text ={this.props.phone}
							onChange= {this.handleChange.bind(this,'phone',cust)}
							style  = {style}
						/>
					</div>
				</div>
				)
	}
	handleChange(field, cust, value){
		this.props.editCustomer({...cust, [field] : value})
	}
}

Customer.propTypes = {
	editCustomer : PropTypes.func.isRequired,
}


const style={
	width:"calc(100% - 60px)", 
	maxWidth:"calc(100% - 60px)", 
	minWidth:150, 
	color:'#394246', 
	fontSize:"1.2em"
}