import React , {PropTypes, Component} from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';

export default class NewCustomerDialog extends Component {
	constructor(props){
		super(props);
		this.state = {
			name:'',
			address: '',
			phone:'',
			err:false,
			active:false,
		}
		this.openDialog = this.openDialog.bind(this);
		this.closeDialog = this.closeDialog.bind(this);
	}
	render (){
		return(<Dialog
					active={this.state.active}
          			onOverlayClick={this.closeDialog}
				>
					<Input 
         				type='text' 
         				label='Name' 
         				name='name'
         				icon='perm_identity'
         				value = {this.state.name}
         				onChange={this.handleCustChange.bind(this, 'name')} 
         				required
         				maxLength={50}
         				onKeyPress = {this.handleKey.bind(this, 'name')}
         				autoFocus
         				error = {this.state.err ? "field is required":null}
         				onFocus = {this.clearError.bind(this)}
         				onBlur = {this.handleBlur.bind(this)}
         			/>
         			<Input 
         				type='text' 
         				label='Address' 
         				name='address'
         				icon='email'
         				value = {this.state.address}
         				onChange={this.handleCustChange.bind(this, 'address')} 
         				onKeyPress = {this.handleKey.bind(this, 'address')} 
         				maxLength={150}
         				onBlur = {this.handleBlur.bind(this)}
         				ref = "address"
         			/>
         			<Input 
         				type='tel' 
         				label='Phone' 
         				name='phone'
         				icon='phone'
         				value = {this.state.phone}
         				onChange={this.handleCustChange.bind(this, 'phone')} 
         				onKeyPress = {this.handleKey.bind(this, 'phone')} 
         				onBlur = {this.handleBlur.bind(this)}
         				ref = 'phone'
         			/>
				</Dialog>)
	}

	openDialog(){
		this.setState({name:'',address: '',phone:'',active:true});
	}
	closeDialog(){
		const {name,address, phone} = this.state;
		if(name){
			this.props.saveCustomer({name,address,phone});
		}
		this.setState({name:'',address: '',phone:'',active:false});
	}
	handleBlur(){
		const {name} = this.state;
		if(!name) this.setState({err:true});
	}


	clearError(){
		this.setState({err:false});
	}
	handleKey(type, e){
		const {which} = e;
		const {name} = this.state;
		if(which ==13){
			const {name} = this.state;
			switch(type){
				case 'name':
					this.refs.address.getWrappedInstance().focus();
					return;
				case 'address':
					this.refs.phone.getWrappedInstance().focus();
					return ;
				case 'phone':
					if(name){
						this.closeDialog();
					}
					return;
					
			}
		}
	}
	
	handleCustChange(type,value){
		this.setState({[type]:value});
	}
}


NewCustomerDialog.propTypes = {
	saveCustomer: PropTypes.func.isRequired
}