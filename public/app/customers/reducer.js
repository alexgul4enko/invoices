import Constances from './Constances'
const customersReducer  = (state = [], action)=>{
	switch (action.type){
		case Constances.INIT_CUSTOMERS :
			return action.rehidrate;
		case Constances.INSERT_CUSTOMER :
			return [...state ,action.rehidrate];
		case Constances.UPDATE_CUSTOMERDATA :
			return state.map(cust=>{
				const {id} = cust;
				if(id == action.rehidrate.id){
					return action.rehidrate;
				}
				return cust;
			});
		default:
			return state;
	}
};

export default customersReducer;