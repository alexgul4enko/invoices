
import React , {PropTypes, Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './actions';
import './invoice.css';
import HTML5Backend from 'react-dnd-html5-backend';
import ProductsList from './components/ProductsList';
import InvoiceData from './components/InvoiceData';
import { DragDropContext } from 'react-dnd';
import DrugERROR from './components/DrugERROR';


@DragDropContext(HTML5Backend)
class EditInvoice extends Component {
	render(){
		return (
				<div className = "Content_divchik" id="editInvoice">
					<InvoiceData
						customers = {objectToArrya(this.props.customers)}
						updateInvoice ={this.props.updateInvoice}
						editCustomer = {this.props.editCustomer}
						inv = {this.props.inv}
						invoice = {this.props.invoice}
						isDrobable ={true}
						products = {this.props.products}
						updateInvoiceProduct = {this.props.updateInvoiceProduct}
						deleteInvoiceProduct = {this.props.deleteInvoiceProduct}
						updateInvoiceDiscount = {this.props.updateInvoiceDiscount}

					/>
					<ProductsList
						products = {this.props.products}
						updateProd={this.props.updateProd}
						createProd = {this.props.createProd}
						addProductToInvoice = {this.addProductToInvoice.bind(this)}

					/>

					<DrugERROR ref = "error"/>
				</div>
			)
	}

	addProductToInvoice(target,source){
		if(!target) return;
		const {inv} = target;
		if(!inv.customer_id) {
			this.refs.error.showError("Specify customer first");
			return;
		} 
		const {id:invoice_id} = inv;
		const {id:product_id} = source;
		const ifAlready = this.props.invoice.filter(pr=>{
			return product_id == pr.product_id
		}).length>0;
		if(ifAlready){
			this.refs.error.showError("U've alredy add this product");
			return;
		}

		this.props.addInvoiceProduct({invoice_id, product_id});
	}


	componentWillMount() {
		const {id} = this.props.routeParams;
		this.props.initData(id);
	}
	componentWillUnmount() {
		if(!this.props.inv.customer_id ){
			this.props.deleteEmptyInvoice(this.props.inv);
		}
	}
}



const  mapStateToProps = (state= {},props) =>({
    products: state.products,
    customers : state.customers,
    invoice : state.invoice,
    inv : state.invoices.filter(inv_ =>(inv_.id == props.routeParams.id))[0]
})

const objectToArrya = (arr=[])=>{
	return arr.reduce((prev, curr)=>{
		const {id, name } = curr;
		return {...prev,[id]:name }
	},{})
}


const mapDispatchToProps = dispatch=> (bindActionCreators(actions,dispatch));


export default connect(mapStateToProps, mapDispatchToProps)(EditInvoice);