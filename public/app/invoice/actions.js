import Constances from './Constances';
import HttpPromice from '../HttpPromice';
import {setError} from '../app.actions';
import {showLoading,dismissLoading} from '../app.actions';

const invoiceActions = {
	initData: id=>{
		const loading = loadContol();
		return (dispatch , getState)=>{
			dispatch(showLoading())
			const {products,customers,invoice} = getState();
			if(isArrEmpty(products)){
				HttpPromice('/api/products',"GET")
					.then(products=>{
						dispatch(initProducts(products));
						if(loading()) dispatch(dismissLoading());
					})
					.catch(err=>{
						dispatch(setError(err && err.message));
						if(loading()) dispatch(dismissLoading());
					})
			}
			else{
				if(loading()) dispatch(dismissLoading());
			}
			if(isArrEmpty(customers)){
				HttpPromice('/api/customers',"GET")
					.then(customers=>{
						dispatch(initCustomers(customers));
						if(loading()) dispatch(dismissLoading());
					})
					.catch(err=>{
						dispatch(setError(err && err.message));
						if(loading()) dispatch(dismissLoading());
					})
			}
			else{
				if(loading()) dispatch(dismissLoading());
			}

			HttpPromice(`/api/invoices/${id}/items`,"GET")
				.then(data=>{
					dispatch(initInvoiceData(data));
					if(loading()) dispatch(dismissLoading());
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
					if(loading()) dispatch(dismissLoading());
				})
		} 
	},
	updateProd:(prod ={})=>{
		return (dispatch , getState)=>{
			const {id} = prod;
			HttpPromice(`/api/products/${id}`,"PUT",prod)
				.then(data=>{
					dispatch(updateProd(data));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	createProd:(prod ={})=>{
		return (dispatch , getState)=>{
			const {id} = prod;
			HttpPromice(`/api/products`,"POST",prod)
				.then(newProd=>{
					dispatch(insertProd(newProd));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},

	updateInvoice:inv=>{
		return (dispatch , getState)=>{
			const {id} = inv;
			HttpPromice(`/api/invoices/${id}`,"PUT",inv)
				.then(updatedInv=>{
					dispatch(updateInvoice(updatedInv));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	updateInvoiceDiscount:inv=>{
		return (dispatch , getState)=>{
			const {id} = inv;
			HttpPromice(`/api/invoices/${id}`,"PUT",inv)
				.then(updatedInv=>{
					dispatch(updateInvoice(updatedInv));
					console.log(calculateTotal(dispatch , getState,updatedInv,'discount'))
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	editCustomer: (cust={})=>{
		return (dispatch , getState)=>{
			const {address,name,phone,inv} =cust;
			if(!inv.customer_id){
				HttpPromice('/api/customers',"POST",{address,name,phone})
					.then(newCustomer=>{
						dispatch(insertCustomer(newCustomer));
						dispatch(invoiceActions.updateInvoice({...inv, customer_id:newCustomer.id}))
					})
					.catch(err=>{
						dispatch(setError(err && err.message));
					})
			}
			else{

				HttpPromice(`/api/customers/${inv.customer_id}`,"PUT",{address,name,phone})
					.then(newCustomer=>{
						dispatch(updateCustomerData(newCustomer));
						
					})
					.catch(err=>{
						dispatch(setError(err && err.message));
					})
			}
		}
	},

	addInvoiceProduct:(prod={})=>{
		return (dispatch , getState)=>{
			const {invoice_id, product_id} = prod;
			HttpPromice(`/api/invoices/${parseInt(invoice_id)}/items`,"POST",{product_id,quantity:0})
				.then(newInvoiceItem=>{
					dispatch(addProductToInvoice(newInvoiceItem));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
			
		}
	},

	updateInvoiceProduct: (prod={})=>{
		return (dispatch , getState)=>{
			const{id,product_id,quantity,invoice_id} = prod;
			
			HttpPromice(`/api/invoices/${invoice_id}/items/${id}`,"PUT",{product_id,quantity})
				.then(newInvoiceItem=>{
					dispatch(changeQTY(newInvoiceItem));
					console.log(calculateTotal(dispatch , getState,prod,'update'))
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	deleteInvoiceProduct: (prod={})=>{
		return (dispatch , getState)=>{
			const{id,product_id,invoice_id} = prod;
			
			HttpPromice(`/api/invoices/${invoice_id}/items/${id}`,"DELETE")
				.then(newInvoiceItem=>{
					dispatch(deleteInvoiceProd(id));
					console.log(calculateTotal(dispatch , getState,prod,'delete'))
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	deleteEmptyInvoice:(inv={})=>{
		return (dispatch , getState)=>{
			const{id} = inv;
			HttpPromice(`/api/invoices/${id}`,"DELETE")
				.then(newInvoiceItem=>{
					dispatch(deleteEmptyInv(id));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	}

}

const deleteEmptyInv =(rehidrate={})=>({
	type:Constances.EDIT_INVOICE_DELETE_EMTY,
	rehidrate,
});


const calculateTotal = (dispatch , getState,prod ={}, action) =>{
	if(action == 'discount'){
		return recalculateDiscount(dispatch , getState,prod );
	}
	const{invoice_id} = prod;
	const {invoice, invoices, products} = getState();
	const cur_invoice = invoices.filter(inv=>{
		const {id} = inv;
		return id ==  invoice_id;
	})[0];
	const {discount,customer_id,id} = cur_invoice;
	const price_obj = prices(products);

	const totalSUM = updatedData(invoice,prod,action).reduce((prev,cur)=>{
		const {quantity, product_id} =cur;
		if(quantity){
			const {[product_id] :pr_pr} = price_obj;
			return prev + ((pr_pr || 0) * quantity )
		}
		return prev;
	},0);
	const total = totalSUM * (1-(discount||0)/100);
	return dispatch(invoiceActions.updateInvoice({total,discount,customer_id,id}));

}

const recalculateDiscount = (dispatch , getState,cur_invoice) =>{
	const {invoice, products} = getState();
	const {discount,customer_id,id} = cur_invoice;
	const price_obj = prices(products);
	const totalSUM = invoice.reduce((prev,cur)=>{
		const {quantity, product_id} =cur;
		if(quantity){
			const {[product_id] :pr_pr} = price_obj;
			return prev + ((pr_pr || 0) * quantity )
		}
		return prev;
	},0);
	const total = totalSUM * (1-(discount||0)/100);
	return dispatch(invoiceActions.updateInvoice({total,discount,customer_id,id}));

}
const updatedData =(invoice =[], action, prod ={})=>{
	switch(action){
		case 'delete':
			return invoice.filter(inv=>{
				const {id} =  inv;
				return id!=prod.id;
			});
		case 'update':
			return invoice.map(inv=>{
				const {id} =  inv;
				if(id == prod.id){
					return {...inv, quantity:prod.quantity}
				}
				return inv;
			});
		default:
			return invoice;
	}

}

const prices = (propducts=[])=>{
	return propducts.reduce((prev,cur)=>{
		const {id, price} = cur;
		return {...prev, [id]:price}
	},{})
}

const changeQTY = (rehidrate={})=>({
	type:Constances.EDIT_INVOICE_CHANGE_QTY,
	rehidrate,
});
const deleteInvoiceProd = (rehidrate={})=>({
	type:Constances.EDIT_INVOICE_DELETE_PROD,
	rehidrate,
});

const addProductToInvoice = (rehidrate={})=>({
	type:Constances.EDIT_INVOICE_ADD_PRODUCT,
	rehidrate,
});

const updateCustomerData = (rehidrate={})=>({
	type:Constances.EDIT_INVOICE_UPDATE_CUSTOMERDATA,
	rehidrate,
});

const updateInvoice = (rehidrate={})=>({
	type:Constances.EDIT_INVOICE_UPDATE_MAINDATA,
	rehidrate,
});

const insertCustomer =  (rehidrate={})=>({
	type:Constances.EDIT_INVOICE_INSERT_CUSTOMER,
	rehidrate,
});
const insertProd =  (rehidrate=[])=>({
	type:Constances.EDIT_INVOICE_INSERT_PROD,
	rehidrate,
});


const finishLoad =()=>{
	console.log("LOADFINISHED")
}

function loadContol(){
	let loadCount = 0;
	return ()=>{
		loadCount++;
		return loadCount==3;
	}
};

const updateProd =  (rehidrate=[])=>({
	type:Constances.EDIT_INVOICE_EDIT_PROD,
	rehidrate,
});

const initProducts = (rehidrate=[])=>({
	type:Constances.EDIT_INVOICE_ADD_PRODUCTSLIST,
	rehidrate,
});
const initCustomers= (rehidrate=[])=>({
	type:Constances.EDIT_INVOICE_ADD_CUSTOMERSLIST,
	rehidrate,
});
const initInvoiceData= (rehidrate=[])=>({
	type:Constances.EDIT_INVOICE_ADD_INVOICE_PRODUCTSLIST,
	rehidrate,
});

const isArrEmpty =(arr =[])=>{
	return arr.length ==0;
}


export default  invoiceActions;