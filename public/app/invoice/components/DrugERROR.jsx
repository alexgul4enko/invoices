import React , {Component, PropTypes} from 'react';
import {  Snackbar } from 'react-toolbox';

export default class DrugERROR extends Component {
	constructor(props){
		super(props);
		this.state= {
			active:false,
			error:''
		}
		this.showError = this.showError.bind(this);
	}

	render(){
		return(
			<Snackbar
				active={this.state.active}
				label={this.state.error}
				type='warning'
				timeout={2000}
          		onTimeout={this.closeError.bind(this)}
			/>
		)
	}

	closeError(){
		this.setState({ active:false});
	}

	showError(error){
		this.setState({error, active:true});
	}

}