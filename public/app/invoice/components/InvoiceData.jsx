import React , {Component, PropTypes} from 'react'
import InvoiceHeader from './InvoiceHeader';
import InvoiceList from './InvoiceList';
import InvoiceFooter from './InvoiceFooter';
import { DropTarget } from 'react-dnd';


const boxTarget = {
  drop(props) {
  	const {inv, invoice} =props;
    return {inv, invoice};
  },
  canDrop(props){
    return props.isDrobable;
  }
};
@DropTarget('product', boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
}))
export default class InvoiceData extends  Component{
	constructor(props){
		super(props);
	}
	render(){
		const { connectDropTarget } = this.props;
		return connectDropTarget(
			<div className = "invoice">
				<InvoiceHeader
					customers = {this.props.customers}
					editCustomer = {this.props.editCustomer}
					updateInvoice = {this.props.updateInvoice}
					inv = {this.props.inv}
					updateInvoiceDiscount = {this.props.updateInvoiceDiscount}
				/>
				<InvoiceList
					invoice = {this.props.invoice}
					inv = {this.props.inv}
					products = {objectToArrya(this.props.products)}
					updateInvoiceProduct = {this.props.updateInvoiceProduct}
					deleteInvoiceProduct = {this.props.deleteInvoiceProduct}
				/>
				<InvoiceFooter
					total = {this.props.inv && this.props.inv.total || 0}
				/>
				
			</div>
		)
	}
}


const objectToArrya = (arr=[])=>{
	return arr.reduce((prev, curr)=>{
		const {id } = curr;
		return {...prev,[id]:curr }
	},{})
}



InvoiceData.propTypes = {
	editCustomer: PropTypes.func.isRequired,
	customers: PropTypes.object.isRequired,
	updateInvoice: PropTypes.func.isRequired,
	invoice: PropTypes.array,
	inv : PropTypes.object.isRequired,
	isDrobable: PropTypes.bool.isRequired,
}