import React , {Component, PropTypes} from 'react'
import {Button} from 'react-toolbox/lib/button';
import { browserHistory } from 'react-router';

export default class InvoiceFooter extends  Component{
	constructor(props){
		super(props);
	}
	render(){
		return(
			<div className = "invoiceFooter">
				<Button 
					icon='share' 
					label='Done' 
					className = "done" 
					onMouseUp = {()=>{browserHistory.push('/')}}
				/>
				<span>{`Total:  ${parseFloat(this.props.total||0).toFixed(2)}$`}</span>
			</div>
		)
	}
}



InvoiceFooter.propTypes = {
	
}