import React , {Component, PropTypes} from 'react';
import {  Snackbar } from 'react-toolbox';
import Autocomplete from 'react-toolbox/lib/autocomplete';
import CustomersDialog from '../../components/CustomersDialog';
import EditText from '../../components/EditText';


export default class InvoiceHeader extends  Component{
	constructor(props){
		super(props);
		this.state = {
			activeBar: false,
			dialog:false,
			err :'',
		}
		this.handleCustomer = this.handleCustomer.bind(this);
		this.tongleDialog = this.tongleDialog.bind(this);
		this.tongleSnack =this.tongleSnack.bind(this);
	}

	render(){
		return(
			<div className = "header">
				<Autocomplete 
					className = "customersSelect"
					direction="down"
					hint="choose customer"
					multiple={false}
					onChange={this.handleCustomer}
					source={this.props.customers}
					value={[this.props.inv.customer_id]}
					suggestionMatch ='anywhere'
					
				/>
			
				<EditText
					onChange = {this.changeDiscount.bind(this)}
					text = {this.props.inv.discount ? this.props.inv.discount +'':''}
					type = 'number'
					after = "%"
					style = {{lineHeight:"43px",width:60,height:43,fontSize: "1rem",marginLeft:10}}
				/>

				<Snackbar
					active={this.state.err!=''}
					label={this.state.err}
					type='warning'
					timeout={2000}
          			onTimeout={this.setError.bind(this,'')}
				/>
				
			
				
				<Snackbar
					active={this.state.activeBar}
					label={'Add new customer?'}
					action = "Add"
					type='warning'
					timeout={3000}
					onClick={this.openCustomersDialog.bind(this)}
          			onTimeout={this.setError.bind(this,'')}
				/>
				<CustomersDialog
					active = {this.state.dialog}
					tongleDialog = {this.tongleDialog}
					inv = {this.props.inv}
					editCustomer = {this.props.editCustomer}
				/>
			</div>
		)
	}

	

	changeDiscount(d){
		if(!this.props.inv.customer_id){
			this.setError('Specify customer first')
			return;
		}
		if(!d) return;
		const discount = parseFloat(d);
		if(discount<0){
			this.setError('Discount should be >0')
		}
		else if(discount>100  ){
			this.setError('Discount should be <100')
		}
		else if(isNaN(discount)){
			this.setError('Something went wrong((')
		}
		else if(!discount && discount!=0){
			
		}
		else{
			this.props.updateInvoiceDiscount({...this.props.inv, discount});
		}

	}
	setError(err){
		this.setState({err});
	}

	handleCustomer(value){
		if(!value){
			this.props.updateInvoice({...this.props.inv, customer_id:null});
			this.tongleSnack();
		}
		else{
			this.props.updateInvoice({...this.props.inv, customer_id:parseInt(value)});
		}
	}
	tongleDialog(){
		this.setState({dialog: !this.state.dialog});
	}
	openCustomersDialog(){
		this.tongleDialog();
		this.tongleSnack()
	}
	tongleSnack(){
		this.setState({activeBar:!this.state.activeBar});
	}

}


InvoiceHeader.propTypes = {
	updateInvoiceDiscount: PropTypes.func.isRequired,
	updateInvoice: PropTypes.func.isRequired,
	editCustomer: PropTypes.func.isRequired,
	customers: PropTypes.object.isRequired,
	inv: PropTypes.shape({
	    customer_id: PropTypes.number,
	    discount: PropTypes.number,
	    total: PropTypes.number,
	    id: PropTypes.number.isRequired,
	  }).isRequired,
}

