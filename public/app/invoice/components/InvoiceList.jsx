import React , {Component, PropTypes} from 'react'
import ListContainer, {List} from '../../components/filtList/ListContainer';
import InvoiceProduct from './InvoiceProduct';

export default class InvoiceList extends  Component{
	constructor(props){
		super(props);
		this.renderEmpty = this.renderEmpty.bind(this);
		this.renderList = this.renderList.bind(this);
	}
	render(){
		return(
			<div className = "invoiceProdList">
				{arrIsEmpty(this.props.invoice)? this.renderEmpty() : this.renderList()}
			</div>
		)
	}

	renderEmpty(){
		return <span className = "Infos">Drag products here</span>
	}
	renderList (){
		const items = this.props.invoice.map(pr=>{
			const {id,product_id,invoice_id,quantity} = pr ;
			const {name, price} = this.props.products && this.props.products[product_id] ||{};
			return {id,product_id,invoice_id,quantity, name, price};
		})
		return (
			<ListContainer>
				<List
					filterPlaceHolder = "search"
					renderItem = {this.renderItem.bind(this)}
					items = {items}
					filter = {this.filterContent}


				/>
			</ListContainer>
		)
	}


	renderItem(prod){
		return (
			<InvoiceProduct
				key = {prod.id}
				addProductQTY = {this.props.updateInvoiceProduct}
				deleteProduct = {this.props.deleteInvoiceProduct}
				{...prod}
			/>
			
		)
	}

	filterContent(item,filt){
		const regEx = new RegExp(filt, 'i');
		return regEx.test(item.name);
	}
}


const arrIsEmpty = (arr=[])=>(arr.length==0)


InvoiceList.propTypes = {
	invoice:PropTypes.array,
}