import React , {Component, PropTypes} from 'react';
import EditText from '../../components/EditText';


export default class InvoiceProduct extends Component {
	constructor(props){
		super (props);
		this.deleteProduct = this.deleteProduct.bind(this);
		this.changeDiscount = this.changeDiscount.bind(this);
	}
	render (){
		return (
			<div className = "InvoiceProduct">
				<span className = "Name">{this.props.name}</span>
				<span className = "price">{`${this.props.price}$`}</span>

				<EditText
					onChange = {this.changeDiscount}
					text = {this.props.quantity ? this.props.quantity +'':''}
					type = 'number'
					style = {{lineHeight:"30px",width:60,height:30,fontSize: "1rem",border:"1px solid #73A3A6" }}
				/>

				<span 
					className = "material-icons" 
					onClick = {this.deleteProduct}
				>
					delete_forever
				</span>
			</div>
		)
	}

	deleteProduct(){
		const {id,product_id,invoice_id} = this.props;
		this.props.deleteProduct({id,product_id,invoice_id});
	} 

	changeDiscount(dd){
		const quantity = parseFloat(dd);
		if(quantity<0) return;
		if(quantity  || quantity == 0){
			const {id,product_id,invoice_id, name, price} = this.props;
			this.props.addProductQTY({id,product_id,invoice_id,quantity, name, price});
		}
	}
}





