import React , {Component, PropTypes} from 'react';
import ListContainer, {List, ListHeader} from '../../components/filtList/ListContainer';
import ProductDND from '../../components/product/ProductDND';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';

export default class ProductsList extends  Component{
	constructor(props){
		super(props);
		this.state={
			active:false,
			name:'',
			price:0
		}
		this.tongleDialog = this.tongleDialog.bind(this);
		this.createProd = this.createProd.bind(this);
	}
	render(){
		return (
			<div className = "products">
				<ListContainer>
					<ListHeader
						title = "Products"
						addData = {this.tongleDialog}
						class = "productsListHeader"
					/>
					<List
						filterPlaceHolder = "search"
						renderItem = {this.renderItem.bind(this)}
						items = {this.props.products}
						filter = {this.filterContent}

					/>
				</ListContainer>
				<Dialog
				    class = "ProductDialog"
					active={this.state.active}
					onEscKeyDown={this.createProd}
					onOverlayClick={this.createProd}
				>
         			<Input 
         				type='text' 
         				label='Name' 
         				name='name'
         				value = {this.state.name}
         				onChange={this.handleProdChange.bind(this, 'name')} 
         				required
         				maxLength={50}
         				onKeyPress = {this.handleKey.bind(this, 'name')} 
         				autoFocus
         			/>
         			<Input 
         				type='number' 
         				label='Price' 
         				name='price'
         				value = {this.state.price}
         				onChange={this.handleProdChange.bind(this, 'price')} 
         				onKeyPress = {this.handleKey.bind(this, 'price')} 
         				required
         				ref = 'price'
         			/>
        		</Dialog>

        		
			</div>
		)
	}

	handleProdChange(type,value_){
		const value = (type== 'name')? value_: parseFloat(value_);
		this.setState({[type]:value});
	}

	handleKey(type, e){
		const {which} = e;
		if(which ==13){
			const {name,price} = this.state;
			if(name!='' && price!=0){
				this.props.createProd({name,price} );
				this.tongleDialog();
			}
			else if(type=='name'){
				this.refs.price.getWrappedInstance().focus();
			}
			
		}

	}

	createProd(){
		const {name,price} = this.state;
		if(name && price){
			this.props.createProd({name,price} );
		}
		this.tongleDialog();
	}


	renderItem(prod,arrKey){
		return (
			<ProductDND 
				key = {prod.id}
				{...prod}
				prod = {prod}
				changeProd = {this.props.updateProd}
				canDrug = {true}
				onDrop = {this.props.addProductToInvoice}
			/>
		)
	}

	


	filterContent(item,filt){
		const regEx = new RegExp(filt, 'i');
		return regEx.test(item.name);
	}

	tongleDialog (){
		this.setState({active:!this.state.active, name:'',price:0});
	}
	
}


ProductsList.propTypes ={
	products:PropTypes.arrayOf(PropTypes.shape({
		    id: PropTypes.number.isRequired,
		    name: PropTypes.string,
		    price:PropTypes.number
		  })
	),
	updateProd:PropTypes.func.isRequired,
	createProd:PropTypes.func.isRequired,
	addProductToInvoice : PropTypes.func.isRequired,
}