import  Constances from './Constances';



const invoicesReducer = (state = [], action)=>{
	switch (action.type){
		case Constances.EDIT_INVOICE_UPDATE_MAINDATA :
			return state.map(inv=>{
				const {id} = inv;
				if(id ==  action.rehidrate.id){
					return action.rehidrate;
				}
				return inv;
			});
		case Constances.EDIT_INVOICE_DELETE_EMTY:
			return state.filter(inv=>{
				const {id } = inv;
				return id!=action.rehidrate;
			})
		default:
			return state;
	}
};

const invoiceReducer = (state = [], action)=>{
	switch (action.type){
		case Constances.EDIT_INVOICE_ADD_INVOICE_PRODUCTSLIST :
			return action.rehidrate;
		case Constances.EDIT_INVOICE_ADD_PRODUCT:
			return [...state, action.rehidrate];
		case Constances.EDIT_INVOICE_CHANGE_QTY:
			return state.map(pr=>{
				const {id}  = pr;
				if(id == action.rehidrate.id){
					return action.rehidrate;
				}
				return pr;
			})
		case Constances.EDIT_INVOICE_DELETE_PROD:
			return state.filter(prod=>{
				const {id} = prod;
				return id!=action.rehidrate;
			})
		default:
			return state;
	}
};

const invoceProducts  = (state = [], action)=>{
	switch (action.type){
		case Constances.EDIT_INVOICE_ADD_PRODUCTSLIST :
			return action.rehidrate;
		case Constances.EDIT_INVOICE_EDIT_PROD:
			const {id} = action.rehidrate;
			return state.map(prod=>{
				if(prod.id == id){
					return action.rehidrate;
				}
				return prod;
			});
		case Constances.EDIT_INVOICE_INSERT_PROD:
			return [...state,action.rehidrate ]
		default:
			return state;
	}
};
const invoceCustomers  = (state = [], action)=>{
	switch (action.type){
		case Constances.EDIT_INVOICE_ADD_CUSTOMERSLIST :
			return action.rehidrate;
		case Constances.EDIT_INVOICE_INSERT_CUSTOMER :
			return [...state ,action.rehidrate];
		case Constances.EDIT_INVOICE_UPDATE_CUSTOMERDATA :
			return state.map(cust=>{
				const {id} = cust;
				if(id == action.rehidrate.id){
					return action.rehidrate;
				}
				return cust;
			});
		default:
			return state;
	}
};



export default invoiceReducer;
export {invoceProducts, invoceCustomers,invoicesReducer};