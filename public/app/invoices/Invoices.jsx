import React , {PropTypes, Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './actions';
import {Button} from 'react-toolbox/lib/button';
import './invoices.css';
import ListContainer, {List} from '../components/filtList/ListContainer';
import InvoiceItem from './components/InvoiceItem';



class Invoices extends Component {
	render(){
		const cust_obj = arrToObj(this.props.customers);
		const items = this.props.invoices.map(inv=>{
			const {customer_id} =  inv;
			const {[customer_id]:cust} = cust_obj;
			return {...inv, cust};
		})
		return (<div className = "Content_divchik" id = "invoices">
					<Button 
						className = "add_invoice"
						icon='add' 
						label='New invoice' 
						flat 
						onMouseUp = {this.props.addInvoice}
						primary />

					<ListContainer class= "invoices">
						<List
							filterPlaceHolder = "search"
							renderItem = {this.renderItem.bind(this)}
							items = {items}
							filter = {this.filterContent}
						/>
					</ListContainer>
				</div>)
	}
	renderItem(item){
		return (
			<InvoiceItem 
				key ={item.id} 
				item = {item}
				deleteInvoice = {this.props.deleteInvoice}
			/>
		)
	}
	filterContent(item,filt){
		const regEx = new RegExp(filt, 'i');
		return regEx.test(JSON.stringify(item));
	}


	componentWillMount() {
		if(!this.props.invoices || !this.props.invoices.length)this.props.initData();
	}
}

const arrToObj =(arr=[])=>{
	return arr.reduce((prev,cur)=>{
		const {id} = cur;
		return {...prev,[id]:cur}
	},{});
}

const  mapStateToProps = (state= {}) =>({
    invoices: state.invoices,
    customers :  state.customers,
})


const mapDispatchToProps = dispatch=> (bindActionCreators(actions,dispatch));


export default connect(mapStateToProps, mapDispatchToProps)(Invoices);