import Constances from './Constances';
import HttpPromice from '../HttpPromice';
import {setError,showLoading,dismissLoading} from '../app.actions';
import { browserHistory } from 'react-router';
import C_Constances from '../customers/Constances'

const invoicesActions = {
	initData: ()=>{
		return (dispatch, getState)=>{
			const loading = loadContol();
			dispatch(showLoading())
			const {customers,invoices} = getState();
			if(isArrEmpty(customers)){
				HttpPromice('/api/customers',"GET")
					.then(customers=>{
						dispatch(initCustomer(customers));
						if(loading()) dispatch(dismissLoading());
					})
					.catch(err=>{
						dispatch(setError(err && err.message));
						if(loading()) dispatch(dismissLoading());
					})
			}
			else{
				if(loading()) dispatch(dismissLoading());
			}
			if(isArrEmpty(invoices)){
				HttpPromice('/api/invoices',"GET")
					.then(invoices=>{
						dispatch(initInvoicesList(invoices));
						if(loading()) dispatch(dismissLoading());
					})
					.catch(err=>{
						dispatch(setError(err && err.message));
						if(loading()) dispatch(dismissLoading());
					})
			}
			else{
				if(loading()) dispatch(dismissLoading());
			}
		}
	},

	addInvoice:()=>{
		return (dispatch, getState)=>{
			HttpPromice('/api/invoices',"POST")
				.then(invoice=>{
					dispatch(addInvoice(invoice));
					browserHistory.push(`/Invoices/${invoice.id}`)
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	deleteInvoice: id=>{
		return (dispatch, getState)=>{
			HttpPromice(`/api/invoices/${id}`,"DELETE")
				.then(invoice=>{
					dispatch(DELL(id));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	}
}

const DELL =rehidrate=>({
	type:Constances.DELL_INVOICE,
	rehidrate
});

const initCustomer= (rehidrate =[])=>({
	type:C_Constances.INIT_CUSTOMERS,
	rehidrate
});

const addInvoice= (rehidrate =[])=>({
	type:Constances.ADD_INVOICE,
	rehidrate
});

const initInvoicesList = (rehidrate =[])=>({
	type:Constances.INIT_INVOICESLIST,
	rehidrate
});

const isArrEmpty =(arr =[])=>{
	return arr.length ==0;
}

function loadContol(){
	let loadCount = 0;
	return ()=>{
		loadCount++;
		return loadCount==2;
	}
};




export default invoicesActions;