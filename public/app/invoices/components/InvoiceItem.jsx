import React , {Component, PropTypes} from 'react';
import { browserHistory } from 'react-router';

export default class InvoiceItem extends Component{
	constructor(props){
		super(props);
		this.editInvpoice = this.editInvpoice.bind(this);
		this.deleteInvoice = this.deleteInvoice.bind(this);
	}
	render(){
		const {item} = this.props;
		return (
			<div className="invoice_item">
				<div className = "invoice-info">
				<Item text = {getCustom(item.cust)} icon = "perm_identity"/>
				<Item text = {getTottal(item)} icon = "attach_money"/>
				<Item text = {getDiscount(item)} icon = "%"/>
				<Item text = {getCreate(item)} icon = "event"/>
				</div>
				<div className = "invoice-actios">
					<span 
						className = "material-icons pen"
						onClick = {this.editInvpoice}
					>
						{"mode_edit"}
					</span>
					<span 
						className = "material-icons trash"
						onClick = {this.deleteInvoice}
					>
						{"delete_forever"}
					</span>
				</div>
			</div>
		)
	}

	editInvpoice(){
		const {id} = this.props.item;
		browserHistory.push(`/Invoices/${id}`)
	}

	deleteInvoice(){
		const {id} = this.props.item;
		this.props.deleteInvoice(id);
	}
}


const getCustom = (cust = {})=>{
	return cust.name ? cust.name :'';
}

const getTottal = (inv = {})=>{
	const {total} = inv;
	return (total || total ==0) ? parseFloat(total).toFixed(2)+' $' :'0.00 $';
}

const getDiscount = (inv = {})=>{
	const {discount} = inv;
	return (discount || discount ==0) ? parseFloat(discount).toFixed(2)+' %' :'0.00 %';
}
const getCreate = (inv = {})=>{
	const {createdAt} = inv;
	const dd = new Date(createdAt);
	const y = dd.getYear()+1900;
	const m = dd.getMonth()+1;
	const d = dd.getDate();
	return `${y}-${m<10?'0':''}${m}-${d<10?'0':''}${d}`
}

const Item = props =>(
	<div> 
		<span className = "material-icons icon">
			{props.icon}
		</span>
		<span className = "text">
			{props.text || ''}
		</span>
	</div>
)
