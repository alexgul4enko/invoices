import Constances from './Constances';

const invoicesReducer  = (state = [], action)=>{
	switch (action.type){
		case Constances.INIT_INVOICESLIST:
			return action.rehidrate;
		case Constances.ADD_INVOICE:
			return [...state ,action.rehidrate] ;
		case Constances.DELL_INVOICE:
			return state.filter(inv=>{
				const {id} = inv;
				return id != action.rehidrate;
			})
		default:
			return state;

	}
}


export default invoicesReducer;