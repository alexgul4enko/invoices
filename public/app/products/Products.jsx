import React , {PropTypes, Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './actions';
import './products.css';
import {Button} from 'react-toolbox/lib/button';
import NewProductDialog from './components/NewProductDialog';
import ListContainer, {List} from '../components/filtList/ListContainer';
import Product from '../components/product/Product';


class Products extends Component {
	constructor(props){
		super(props);
		this.openProductDialod  = this.openProductDialod.bind(this);
	}
	render(){
		return (
			<div className = "Content_divchik" id = "products">
				<Button 
					className = "add_product"
					icon='add' 
					label='New product' 
					flat 
					onMouseUp = {this.openProductDialod}
					primary 
				/>
				<ListContainer class = "product-list">
					<List
						filterPlaceHolder = "search"
						renderItem = {this.renderItem.bind(this)}
						items = {this.props.products}
						filter = {this.filterContent}

					/>
				</ListContainer>

				<NewProductDialog 
					ref = "product_dialog" 
					createProd = {this.props.createProd}
				/>
			</div>
		)
	}
	

	openProductDialod(){
		this.refs.product_dialog.tongleDialog();
	}
	renderItem(prod,arrKey){
		return (
			<Product
				key = {prod.id}
				{...prod}
				prod = {prod}
				changeProd = {this.props.updateProd}
			/>
		)
	}
	filterContent(item,filt){
		const regEx = new RegExp(filt, 'i');
		return regEx.test(item.name);
	}

	componentWillMount() {
		if(isArrEmpty(this.props.products)) this.props.initData();
	}
}

const isArrEmpty = (arr=[])=>{
	return arr.length ==0;
}


const  mapStateToProps = (state= {}) =>({
    products: state.products,
})


const mapDispatchToProps = dispatch=> (bindActionCreators(actions,dispatch));


export default connect(mapStateToProps, mapDispatchToProps)(Products);