import Constances from './Constances';
import HttpPromice from '../HttpPromice';
import {setError} from '../app.actions';
import {showLoading,dismissLoading} from '../app.actions';
const prodActions = {
	initData: ()=>{
		return (dispatch, getState)=>{
			dispatch(showLoading());
			HttpPromice('/api/products',"GET")
				.then(products=>{
					dispatch(initProducts(products));
					dispatch(dismissLoading());
				})
				.catch(err=>{
					dispatch(dismissLoading());
					dispatch(setError(err && err.message));
				})
		}

	},
	createProd:(prod ={})=>{
		return (dispatch , getState)=>{
			const {id} = prod;
			HttpPromice(`/api/products`,"POST",prod)
				.then(newProd=>{
					dispatch(insertProd(newProd));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	},
	updateProd:(prod ={})=>{
		return (dispatch , getState)=>{
			const {id} = prod;
			HttpPromice(`/api/products/${id}`,"PUT",prod)
				.then(data=>{
					dispatch(updateProd(data));
				})
				.catch(err=>{
					dispatch(setError(err && err.message));
				})
		}
	}
}

const insertProd =(rehidrate=[])=>({
	type: Constances.ADD_PRODUCT,
	rehidrate
});

const updateProd =(rehidrate=[])=>({
	type: Constances.EDIT_PRODUCT,
	rehidrate
});

const initProducts =(rehidrate=[])=>({
	type: Constances.INIT_PRODUCTS,
	rehidrate
});



export default prodActions;