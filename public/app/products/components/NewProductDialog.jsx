import React , {PropTypes, Component} from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';

export default class NewProductDialog extends Component{
	constructor(props){
		super(props);
		this.state = {
			active:false,
			name:'',
			price:0
		}
		this.createProd = this.createProd.bind(this);
		this.tongleDialog = this.tongleDialog.bind(this);
	}

	render(){
		return (
			<Dialog
				class = "ProductDialog"
				active={this.state.active}
				onEscKeyDown={this.createProd}
				onOverlayClick={this.createProd}
			>
         		<Input 
         			type='text' 
         			label='Name' 
         			name='name'
         			value = {this.state.name}
         			onChange={this.handleProdChange.bind(this, 'name')} 
         			required
         			maxLength={50}
         			onKeyPress = {this.handleKey.bind(this, 'name')} 
         			autoFocus
         		/>
         		<Input 
         			type='number' 
         			label='Price' 
         			name='price'
         			value = {this.state.price}
         			onChange={this.handleProdChange.bind(this, 'price')} 
         			onKeyPress = {this.handleKey.bind(this, 'price')} 
         			required
         			ref = 'price'
         		/>
        	</Dialog>

		)
	}

	tongleDialog(){
		this.setState({active:!this.state.active, name:'',price:0});
	}
	createProd(){
		const {name,price} = this.state;
		if(name && price){
			this.props.createProd({name,price} );
		}
		this.tongleDialog();
	}
	handleProdChange(type,value_){
		const value = (type== 'name')? value_: parseFloat(value_);
		this.setState({[type]:value});
	}
	handleKey(type, e){
		const {which} = e;
		if(which ==13){
			const {name,price} = this.state;
			if(name!='' && price!=0){
				this.props.createProd({name,price} );
				this.tongleDialog();
			}
			else if(type=='name'){
				this.refs.price.getWrappedInstance().focus();
			}
			
		}

	}
}

NewProductDialog.propTypes ={
	createProd: PropTypes.func.isRequired,
}