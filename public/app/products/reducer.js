import Constances from './Constances';


const productReducer = (state = [], action)=>{
	switch(action.type){
		case Constances.INIT_PRODUCTS :
			return action.rehidrate;
		case Constances.EDIT_PRODUCT :
			return state.map(prod=>{
				const {id} = prod;
				if(id == action.rehidrate.id){
					return action.rehidrate;
				}
				return prod;
			})
		case Constances.ADD_PRODUCT :
			return [...state, action.rehidrate]

		default :
			return state;
	}
}

export default productReducer;
