import Constances from './app.Constances';
import {combineReducers}  from 'redux';
import { routerReducer } from 'react-router-redux';
import joinReducers from './joinReducers';
import products from './products/reducer';
import invoices from './invoices/reducer';
import invoice , {invoceProducts,invoceCustomers,invoicesReducer} from './invoice/reducer';
import customers from './customers/reducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  err,
  loading,
  products: joinReducers(products,invoceProducts),
  customers:joinReducers(customers,invoceCustomers),
  invoices: joinReducers(invoices, invoicesReducer),
  invoice
});

function err (state ='', action){
	switch(action.type){
		case Constances.SHOW_ERR:
			return action.rehidrate;
		default:
			return state;
	}
}

function loading (state =false, action){
	switch(action.type){
		case Constances.SHOW_LOADING:
			return true;
		case Constances.DISMISS_LOADING:
			return false;
		default:
			return state;
	}
}



export default rootReducer;