import React  from 'react' 
import { Route, Router,IndexRoute} from 'react-router'
import App from "./App";
import Products from './products/Products';
import Invoices from './invoices/Invoices';
import EditInvoice from './invoice/EditInvoice';
import Customers from './customers/Customers';

export default (
	<Route path="/" component={App}>
		<IndexRoute component={Invoices} />
		<Route path="Products" component = {Products} />
		<Route path="Invoices" component = {Invoices} />
		<Route path="Customers" component = {Customers} />
		<Route path="/Invoices/:id" component = {EditInvoice} />
	</Route>
);